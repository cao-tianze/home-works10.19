package com.powernode.homeworks;

/**
 * 描述 2.编写两个线程,一个线程打印1-52的整数，另一个线程打印字母A-Z。打印顺序为12A34B56C….5152Z。
 * 即按照整数和字母的顺序从小到大打印，并且每打印两个整数后，打印一个字母，交替循环打印，直到打印到整数52和字母Z结束。
 * 作者 曹谨谦
 * 时间 2023/10/18 15:37
 */
class Printer{
    private int index = 1 ;

    /**
     * 打印数字
     */
    public synchronized void numPrint(int i){
            while (index % 3 == 0) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(i);
            index++;
            this.notifyAll();
    }

    /**
     * 打印字母
     */
    public synchronized void letterPrint(char c){
            while (index % 3 != 0) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(c);
            index++;
            this.notifyAll();
    }
}
public class HomeWork2 {
    public static void main(String[] args) {
        Printer p = new Printer();
        new Thread(()->{
            for (int i = 1; i <=52 ; i++) {
                p.numPrint(i);
            }
        }).start();
        new Thread(()->{
            for (int i = 0; i <26 ; i++) {
                p.letterPrint((char)('A'+i));
            }
        }).start();
    }
}
