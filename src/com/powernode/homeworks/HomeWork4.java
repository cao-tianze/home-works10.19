package com.powernode.homeworks;

import java.util.concurrent.TimeUnit;

/**
 * 描述 2.编写多线程程序，模拟多个人通过一个山洞的模拟。这个山洞每次只能通过一个人，
 * 每个人通过山洞的时间为5秒，有10个人同时准备过此山洞，显示每次通过山洞人的姓名和顺序。
 * 作者 曹谨谦
 * 时间 2023/10/19 18:35
 */
class ThroughCave implements Runnable{
    private int count;
    @Override
    public synchronized void run() {
        try {
            TimeUnit.SECONDS.sleep(5);
            count++;
            System.out.println(Thread.currentThread().getName()+"第"+count+"个通过了山洞");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
public class HomeWork4 {
    public static void main(String[] args) {
        Runnable throughCave = new ThroughCave();
        for (int i =1;i<=10;i++) {
            new Thread(throughCave,"线程"+i).start();
        }
    }
}
