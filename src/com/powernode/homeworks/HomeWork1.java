package com.powernode.homeworks;

/**
 * 描述 1.设计一个多线程的程序如下：设计一个火车售票模拟程序。假如火车站要有100张火车票要卖出，
 * 现在有5个售票点同时售票，用5个线程模拟这5个售票点的售票情况。
 * 作者 曹谨谦
 * 时间 2023/10/18 15:37
 */
class TrainTicket implements Runnable{
    private int num = 100;
    @Override
    public void run() {
            for (int i = 1; i <=100 ; i++) {
                synchronized (this) {
                    try {
                    if (num >= 1) {
                        Thread.sleep(10);
                        num--;
                        System.out.println(Thread.currentThread().getName() + ":恭喜购票成功!余票剩余" + num);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

public class HomeWork1 {
    public static void main(String[] args) {
        Runnable ticketTicket = new TrainTicket();
        for (int i =1;i<=5;i++) {
            new Thread(ticketTicket,"线程"+i).start();
        }
    }
}
