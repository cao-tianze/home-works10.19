package com.powernode.homeworks;

/**
 * 描述 1.设计4个线程，其中两个线程每次对j增加1，另外两个线程对j每次减少1。
 * 要求：使用内部类实现线程，对j增减的时候不考虑顺序问题。
 * 作者 曹谨谦
 * 时间 2023/10/19 18:13
 */
public class HomeWork3 {
    private static int j;
    public static void main(String[] args) {
        new Thread(()-> {
            for (int i = 1;i<=100;i++) {
                j++;
                System.out.println(j);
            }
        }).start();
        new Thread(()-> {
            for (int i = 1;i<=100;i++) {
                j++;
                System.out.println(j);
            }
        }).start();
        new Thread(()-> {
            for (int i = 1;i<=100;i++) {
                j--;
                System.out.println(j);
            }
        }).start();
        new Thread(()-> {
            for (int i = 1;i<=100;i++) {
                j--;
                System.out.println(j);
            }
        }).start();
    }
}
